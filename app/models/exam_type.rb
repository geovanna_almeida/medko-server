class ExamType < ActiveRecord::Base
	has_many :user
	has_many :service_window
	has_many :attendence

	has_attached_file :image_url,default_url: "/assets/plusverde.png", 
    styles: { medium: "300x300>", thumb: "100x100>" }

end
