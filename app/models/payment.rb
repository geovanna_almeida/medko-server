class Payment < ActiveRecord::Base
	has_many :attendence
	belongs_to :user
	belongs_to :payment_period, :foreign_key => "payment_period_id"
  	belongs_to :receipt_period, :foreign_key => "receipt_period_id"


   def self.search_payment(query)
     joins("LEFT JOIN payment_periods ON payments.payment_period_id = payment_periods.id 
     	LEFT JOIN receipt_periods ON payments.receipt_period_id = receipt_periods.id")
     .where("cast(payment_periods.start_date as text) like ? 
     	OR cast(payment_periods.end_date as text) like? 
     	OR cast(payment_periods.value as text) like?
     	OR cast(price as text) like?
     	OR cast(receipt_periods.start_date as text) like? 
     	OR cast(receipt_periods.end_date as text) like?
     	OR cast(receipt_periods.value as text) like?",
     	 "%#{query}%","%#{query}%","%#{query}%","%#{query}%","%#{query}%","%#{query}%","%#{query}%").limit(10)
    end

  
def self.month_valor_payment
			user = User.where('account_type_id = 3')
			value = 0
		    total = 0

	for doctor in user do
		if doctor.attendences_as_doctor != nil
		   	  value = doctor.attendences_as_doctor.collect{|a| a.value if a.attendence_status_id == 2 and a.attendence_type_id == 2 }
		      total = value.compact.sum
			  if total != 0
		        payment_period = PaymentPeriod.new
				payment_period.start_date = Date.today.at_beginning_of_month
				payment_period.end_date =  Date.today.end_of_month 
				payment_period.value = total * 0.06
				payment_period.save

		        payment = Payment.new	
				payment.price = total
				payment.user_id = doctor.id
				payment.payment_period_id = payment_period.id
				payment.save
			  end	
			end	
	     end
	  end		


	def self.month_valor_pagseguro	
		user = User.where('account_type_id = 3')
			value = 0
		    total = 0
		for doctor in user do
			if doctor.attendences_as_doctor != nil
		      value = doctor.attendences_as_doctor.collect{|a| a.value if a.attendence_status_id == 2 and a.attendence_type_id == 1 }
		      total = value.compact.sum
	     		payment = total *  0.06
		     	if total != 0
					receipt_period = ReceiptPeriod.new
					receipt_period.start_date = Date.today.at_beginning_of_month
					receipt_period.end_date =  Date.today.end_of_month 
					receipt_period.value = total - payment
					receipt_period.save 

					payment = Payment.new	
					payment.receipt = total
					payment.user_id = doctor.id
					payment.receipt_period_id = receipt_period.id
					payment.save
				end		
	       end
		end		
	end	
end



