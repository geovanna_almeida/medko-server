class ServiceWindow < ActiveRecord::Base

  belongs_to :user
  belongs_to :place
  has_and_belongs_to_many :health_plan
  has_and_belongs_to_many :payment_method
  belongs_to :exam_type
  belongs_to :speciality
  has_many :attendence,:dependent => :destroy
  acts_as_mappable :through => :place

  
  #acts_as_mappable :through => :place

  accepts_nested_attributes_for :place
  accepts_nested_attributes_for :health_plan
  accepts_nested_attributes_for :payment_method
  accepts_nested_attributes_for :exam_type
  accepts_nested_attributes_for :speciality

  validates :speciality,:presence => {:message => "^Escolha pelo menos uma especialidade ou um exame"}, :if => "exam_type == nil"
  validates :exam_type,:presence => {:message => "^Escolha pelo menos um tipo de exame ou uma especialidade"}, :if => "speciality == nil"
  validates :place,:presence => {:message => "^Escolha o local"}
  validates :value,:presence => {:message => "^Digite o valor do atendimento"}
  validates :health_plan,:presence => {:message => "^Escolha um plano ou forma de pagamento"}, :if => "payment_method_ids == nil"
  validates :payment_method,:presence => {:message => "^Escolha um plano ou forma de pagamento"}, :if => "health_plan_ids == nil"



    def take_time
    times = Array.new
    data = Date.today
    hora = (start_time.hour).to_i
    minutos = (start_time.min).to_i
    start =  data + minutos.minutes + hora.hour
    time = start
  
    if Date.today.cwday == week_day
      while times.size < 6 
        if attendence_ids.none? == false
          for attendence_date in attendence do
            if  attendence_date.date != nil
              if time.strftime("%H:%M") <= end_time.strftime("%H:%M")
                time += service_time.minutes
                  if time > DateTime.now - 3.hour and time != attendence_date.date
                    times.push(time) 
                  end
              else
                start += 7.days
                time = start  
                  if time != attendence_date.date
                    times.push(time)
                  end  
              end  
            end  
          end 
        else
            if time.strftime("%H:%M") < end_time.strftime("%H:%M")
              time += service_time.minutes
                if time > DateTime.now - 3.hour
                  times.push(time) 
                end
            else 
              start += 7.days
              time = start  
              times.push(time)
            
          end  
        end
      end  
   else
      start = data.next_week.advance(:days=> week_day - 1) + minutos.minutes + hora.hour
      time = start 
        while times.size < 6 
          if attendence_ids.none? == false 
            for attendence_date in attendence do 
              if time.strftime("%H:%M") < end_time.strftime("%H:%M") and attendence_date.date != time
                time += service_time.minutes
                if   time > DateTime.now - 3.hour and time != attendence_date.date
                  times.push(time)  
                end
              else    
                start += 7.days
                time = start  
                times.push(time) 
              end  
            end 
          else 
            if time.strftime("%H:%M") < end_time.strftime("%H:%M")
              time += service_time.minutes
              times.push(time) 
            else    
              start += 7.days
              time = start  
              times.push(time) 
            end  
          end  
        end  
      end  
    return times         
  end 
end
