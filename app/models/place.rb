class Place < ActiveRecord::Base
		  
  belongs_to :user
  has_many :service_window,:dependent => :destroy
  has_many :attendence, :dependent => :destroy

    acts_as_mappable :default_units => :kms,
                   :default_formula => :sphere,
                   :distance_field_name => :distance,
                   :lat_column_name => :latitude,
                   :lng_column_name => :longitude

end
