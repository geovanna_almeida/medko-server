  class User < ActiveRecord::Base
    # Include default devise modules. Others available are:
    # :confirmable, :lockable, :timeoutable and :omniauthable

  acts_as_mappable :default_units => :kms,
                   :default_formula => :sphere,
                   :distance_field_name => :distance,
                   :lat_column_name => :latitude,
                   :lng_column_name => :longitude


    devise :database_authenticatable, :registerable,
           :recoverable, :rememberable, :trackable, :validatable,
           :omniauthable, :omniauth_providers => [:facebook]
           
    belongs_to :account_type
    has_many :bank_account
    has_many :device_id
    has_many :search_address
    has_many :place
    has_many :service_window
    has_many :payment_method
    has_many :payment_period
    has_many :classification
    has_one :payment
    has_and_belongs_to_many :speciality
    has_and_belongs_to_many :exam_type
    has_many  :attendences_as_patient, :class_name => "Attendence", :foreign_key => "patient_id"
    has_many  :attendences_as_doctor, :class_name => "Attendence", :foreign_key => "doctor_id"

    accepts_nested_attributes_for :bank_account
    accepts_nested_attributes_for :speciality
    accepts_nested_attributes_for :exam_type 
  
    has_attached_file :avatar,   default_url: "/assets/people.png",
    styles: { medium: "300x300>", thumb: "100x100>"}
    validates_attachment :avatar,
                        content_type: { content_type: ["image/jpeg", "image/gif", "image/png"] }

    has_attached_file :crm_image, default_url: "/assets/new.png",
      styles: { medium: "300x300>", thumb: "100x100>" }

    validates_attachment :crm_image,
                         content_type: { content_type: ["image/jpeg", "image/gif", "image/png"] }
                       

    has_attached_file :proof_of_address_image,default_url: "/assets/new.png",
    styles: { medium: "300x300>", thumb: "100x100>" }

    validates_attachment :proof_of_address_image,
                         content_type: { content_type: ["image/jpeg", "image/gif", "image/png"] }   



   def self.search_user(query)
     joins("JOIN account_types ON account_type_id = account_types.id").where("account_types.name like ? OR telephone like? OR email like? OR blocked_message like? OR users.name like?", "%#{query}%","%#{query}%","%#{query}%","%#{query}%","%#{query}%").limit(10).order("name DESC").select('distinct users.*')
    end

    def self.search_between(type)
       where("cast(account_type_id as text) like ?", "%#{type}%").limit(50).order("name DESC")
    end

   def self.search_clinic(exam_type_id,payment_id,speciality_id,health_plan_id,latitude,longitude,patient_id)
    if health_plan_id != "" && exam_type_id != ""
      service_window = ServiceWindow.joins("JOIN health_plans_service_windows on service_windows.id = health_plans_service_windows.service_window_id JOIN places ON service_windows.place_id = places.id").where("cast(exam_type_id as text) like? AND cast(health_plans_service_windows.health_plan_id as text) like?","%#{exam_type_id}%","%#{health_plan_id}%").by_distance(:origin => [latitude,longitude]).order(start_time:  :asc, value: :desc)
    elsif health_plan_id != "" && speciality_id != ""
      service_window = ServiceWindow.joins("JOIN health_plans_service_windows on service_windows.id = health_plans_service_windows.service_window_id JOIN places ON service_windows.place_id = places.id").where("cast(speciality_id as text) like? AND cast(health_plans_service_windows.health_plan_id as text) like?","%#{speciality_id}%","%#{health_plan_id}%").by_distance(:origin => [latitude,longitude]).order(start_time:  :asc, value: :desc)
    elsif payment_id != "" && exam_type_id != ""
       service_window = ServiceWindow.joins("JOIN payment_methods_service_windows on service_windows.id = payment_methods_service_windows.service_window_id JOIN places ON service_windows.place_id = places.id").where("cast(exam_type_id as text) like? AND cast(payment_methods_service_windows.payment_method_id as text) like?","%#{exam_type_id}%","%#{payment_id}%").by_distance(:origin => [latitude,longitude]).order(start_time:  :asc, value: :desc)
    elsif payment_id != "" && speciality_id != ""
        service_window = ServiceWindow.joins("JOIN payment_methods_service_windows on service_windows.id = payment_methods_service_windows.service_window_id JOIN places ON service_windows.place_id = places.id").where("cast(speciality_id as text) like? AND cast(payment_methods_service_windows.payment_method_id as text) like?","%#{speciality_id}%","%#{payment_id}%").by_distance(:origin => [latitude,longitude]).order(start_time:  :asc, value: :desc)
    end
       return service_window
   end  

  def media
    values = Array.new
    media = 0
    for attendence in attendences_as_doctor do
      if attendence.classification != nil
      values.push(attendence.classification.value)
    end
      end 

        if values.size != 0
          media = values.sum / values.size
          return media
        else
           return media 
        end
  end 

    def self.from_omniauth(auth)
        if self.where(email: auth.info.email).exists?
          user = self.where(email: auth.info.email).first
        else  
        where(provider: auth.provider, uid: auth.uid).first_or_create do |user|
        user.name = auth.info.name
        user.email = auth.info.email
        user.password = Devise.friendly_token[0,20]
        user.facebook_token = auth.credentials.token
            if auth.info.image.present?
              avatar_url = process_uri(auth.info.image)
              user.update_attribute(:avatar, URI.parse(avatar_url))
            end
        user.save
      end
      end
    end
    
    def self.new_with_session(params, session)
      if session["devise.user_attributes"]
        new(session["devise.user_attributes"], without_protrection: true) do |user|
          user.attributes = params
          user.valid?
        end
      else
        super
      end
    end

    def password_required?
      super && provider.blank? 
    end

   def update_with_password(params, *options)
        if password.blank?              
            update_attributes(params, *options)
        else
            super
        end
    end

     def self.process_uri(uri)
    require 'open-uri'
    require 'open_uri_redirections'
    open(uri, :allow_redirections => :safe) do |r|
      r.base_uri.to_s
    end
  end

    
    def active_for_authentication?
       active
    end


    def inactive_message
      "Não é possivel realizar o seu acesso. Verifique com a administração do Medko"
    end

end

