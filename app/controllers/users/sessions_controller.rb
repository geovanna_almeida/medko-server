class Users::SessionsController < Devise::SessionsController

  # GET /resource/sign_in
   def new
    super
    omniauth = request.env["omniauth.auth"]
   end

def after_sign_out_path_for(resource_or_scope)
  user_session_path
end

  def after_sign_in_path_for(resource_or_scope)
     if current_user.account_type_id != 2 
     root_path
    else
      users_path
    end
  end

  # POST /resource/sign_in
  def create
    super
    omniauth = request.env["omniauth.auth"]

    if omniauth
      user = User.find_by_provider_and_uid(omniauth['provider'], omniauth['uid'])
      if user
        flash[:notice] = "Signed in successfully."
        sign_in_and_redirect(:user, user)
      #Já logado

      elsif current_user
        user.update_attributes!(:provider => omniauth['provider'], :uid => omniauth['uid'])
        flash[:notice] = "Authentication successful."
        redirect_to root_url
      else
        user = User.new
        user.email = omniauth['user_info']['email'] if user.email.blank?
        if user.save
          flash[:notice] = "Cadastrado com sucesso"
          sign_in_and_redirect(:user, user)
        else
          session[:omniauth] = omniauth.except('extra')
          redirect_to new_user_registration_url
        end
      end
    end
  end
end
