class Api::HealthPlansController < Api::BaseController


  respond_to :json
  
  def index
    @health_plans = HealthPlan.all
    render 'health_plans/index.json.jbuilder'

  end

end