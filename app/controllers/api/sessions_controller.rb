class Api::SessionsController < Api::BaseController
  include Devise::Controllers
  
  before_filter :ensure_params_exist,only: [:create]
  skip_before_action :authenticate_user!

  respond_to :json

  def create
    resource = User.find_for_database_authentication(email: params[:email])
    return invalid_login_attempt unless resource

    if resource.valid_password?(params[:password])
      sign_in(resource)
      session[:user_id] = resource.id
      render json: {success: true,email: resource.email},status:200
      return
    end
    invalid_login_attempt
  end

  def create_facebook
    token = params[:user][:facebook_token]
    return invalid_facebook_login_attempt unless is_facebook_token_valid?(token)

    user = User.find_by(email: params[:user][:email])
    if user == nil
      resource = User.new(user_params)
      resource.password = token
      resource.password_confirmation = token
      resource.active = true
      resource.save
      if resource.persisted?
        sign_in(resource)
        session[:user_id] = resource.id
        render json: {success: true,email: resource.email},status:200
        return
      else
        logger.info resource.errors.full_messages
        invalid_facebook_login_attempt
      end
    else
      logger.info "Account Type"
      logger.info user.account_type
      if user.account_type != nil && user.account_type.id = 1
        user.password = token
        user.password_confirmation = token
        user.save
        sign_in(user)
        session[:user_id] = user.id
        render json: {success: true,email: user.email},status:200
        return
      else
        invalid_facebook_login_attempt
      end
    end

  end

  def destroy
    current_user = User.find_by(id: session[:user_id])
    if current_user
      sign_out(current_user)
      DeviceId.destroy_device_id_by_session(session)
    end
    render json: {logout: true}
  end

  def reset_password
    user_aux = User.new
    user_aux.email = params[:email]
    @user = User.send_reset_password_instructions(user_aux)
    if @user
      if @user.id
          head :status => 200
       else
         head :status => 404
       end
     else
       render :status => 422, :json => { :errors => @user.errors.full_messages }
     end
  end

  protected
  def ensure_params_exist
    return unless params[:email].blank?
    return unless params[:password].blank?
    render json: {success: false,message: "missing  parameters"},status: 422
  end

  def invalid_login_attempt
    warden.custom_failure!
    render json: {success: false, message: "Error with your login or password"},status: 401
  end

  def invalid_facebook_login_attempt
    warden.custom_failure!
    render json: {success: false, message: "Error with your Facebook access"},status: 401
  end

  
  def is_facebook_token_valid?(facebook_token)
    response = RestClient.get 'https://graph.facebook.com/me?fields=id&access_token=' + facebook_token
    true unless response.code != 200
  end
  
  def user_params
      params.require(:user).permit(:id, :avatar, :name, :email, :password, :password_confirmation, :telephone, :crm, :crm_image, :proof_of_address_image, :bank, :full_name, :active, :blocked, :blocked_message, :account_type_id, :device_id, :facebook_token, :facebook_avatar_url, :facebook_account, :avatar_image, :bank_account)
  end


end
