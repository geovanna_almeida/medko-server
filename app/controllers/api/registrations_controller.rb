class Api::RegistrationsController < Api::BaseController
  include Devise::Controllers

  skip_before_action :authenticate_user!


  # POST /resource
  def create
    resource = User.new(register_params)
    resource.save
    if resource.persisted?
       sign_in(resource)
       logger.info resource.errors.full_messages
       session[:user_id] = resource.id
       render json: {success: true,email: resource.email},status:200
       return
    else
      invalid_signup_attempt(resource)
    end
  end

  # POST /resource
  def create_multipart
    resource = User.new(register_multipart_params)
    resource.save
    if resource.persisted?
       sign_in(resource)
       logger.info resource.errors.full_messages
       session[:user_id] = resource.id
       render json: {success: true,email: resource.email},status:200
       return
    else
      invalid_signup_attempt(resource)
    end
  end

  private

    # Never trust parameters from the scary internet, only allow the white list through.
   def register_params
      params.require(:user).permit(:id,:value_home_care,:address,:zip_code, :avatar, :name, :rating, :email, :password, :password_confirmation, :telephone, :crm, :crm_image, :proof_of_address_image, :bank, :full_name, :active, :blocked, :blocked_message, :account_type_id, :device_id, :facebook_token, :facebook_avatar_url, :facebook_account,:avatar_image,exam_type_attributes:[:id],speciality_attributes:[:id], bank_account_attributes:[:id, :bank, :full_name, :cpf_cnpj, :account, :operation, :user,:agency])
   end

   def register_multipart_params
      params.require(:user).permit(:avatar,:value_home_care,:address,:zip_code, :name, :email,:rating, :password, :password_confirmation, :telephone, :crm, :crm_image, :proof_of_address_image,{:exam_type_ids => []},{:speciality_ids => []},:bank, :full_name,:active,:blocked, :blocked_message, :account_type_id, :rating,bank_account_attributes:[:bank, :full_name, :cpf_cnpj,:account,:operation,:user,:agency])
   end

  def invalid_signup_attempt(resource)
    logger.info resource.errors.full_messages
    warden.custom_failure!
    render json: resource.errors ,status: 401
  end

end