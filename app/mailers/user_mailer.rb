class UserMailer < ApplicationMailer

  def attendence_status_change_mail(user, message, attendence)
    @user = user
    @message = message
    @url  = "http://52.67.139.39:3000/attendences/"<< attendence.id
    mail(to: user.email, subject: 'Medko')
  end

  def user_status_change_mail(user, message)
    @user = user
    @message = message
    @url  = "http://52.67.139.39:3000/users/" + user.id
    mail(to: @user.email, subject: 'Medko')
  end

end
