json.array!(@service_windows) do |service_window|

  json.extract! service_window, :id, :week_day, :service_time,:start_time,:end_time,:value

  json.url service_window_url(service_window,  format: :json)

        json.take_time service_window.take_time[0].strftime("%d/%m/%Y - %H:%M")

  		  json.health_plan service_window.health_plan	

        json.payment_method service_window.payment_method
      	
        json.place do |json|
            json.id service_window.place.id
            json.name service_window.place.name
            json.address service_window.place.address
            json.complement service_window.place.complement
            json.latitude   service_window.place.latitude
            json.longitude service_window.place.longitude

        end

      if  service_window.speciality_id != nil
        json.speciality do |json|
            json.id service_window.speciality.id
            json.name service_window.speciality.name
        end 
       end 

      if  service_window.exam_type_id != nil
        json.exam_type do |json|
            json.id service_window.exam_type.id
            json.name service_window.exam_type.name
        end
       end 

        json.user do |json|
            json.id service_window.user.id
            json.name service_window.user.name
            json.telephone service_window.user.telephone
            json.crm service_window.user.crm
            json.rating service_window.user.rating
            json.email service_window.user.email
            json.media service_window.user.media
            json.latitude service_window.user.latitude
            json.longitude service_window.user.longitude

        end

end