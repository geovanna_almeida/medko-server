require 'test_helper'

class AttendencesControllerTest < ActionController::TestCase
  setup do
    @attendence = attendences(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:attendences)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create attendence" do
    assert_difference('Attendence.count') do
      post :create, attendence: { attendece_status_id: @attendence.attendece_status_id, attendence_type_id: @attendence.attendence_type_id, date: @attendence.date, doctor_id: @attendence.doctor_id, exam_tye_id: @attendence.exam_tye_id, home_care_service_id: @attendence.home_care_service_id, patient_id: @attendence.patient_id, payment_id: @attendence.payment_id, payment_method_id: @attendence.payment_method_id, place_id: @attendence.place_id, search_address_id: @attendence.search_address_id, service_window_id: @attendence.service_window_id, speciality_id: @attendence.speciality_id }
    end

    assert_redirected_to attendence_path(assigns(:attendence))
  end

  test "should show attendence" do
    get :show, id: @attendence
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @attendence
    assert_response :success
  end

  test "should update attendence" do
    patch :update, id: @attendence, attendence: { attendece_status_id: @attendence.attendece_status_id, attendence_type_id: @attendence.attendence_type_id, date: @attendence.date, doctor_id: @attendence.doctor_id, exam_tye_id: @attendence.exam_tye_id, home_care_service_id: @attendence.home_care_service_id, patient_id: @attendence.patient_id, payment_id: @attendence.payment_id, payment_method_id: @attendence.payment_method_id, place_id: @attendence.place_id, search_address_id: @attendence.search_address_id, service_window_id: @attendence.service_window_id, speciality_id: @attendence.speciality_id }
    assert_redirected_to attendence_path(assigns(:attendence))
  end

  test "should destroy attendence" do
    assert_difference('Attendence.count', -1) do
      delete :destroy, id: @attendence
    end

    assert_redirected_to attendences_path
  end
end
