require 'test_helper'

class HomeCareServicesControllerTest < ActionController::TestCase
  setup do
    @home_care_service = home_care_services(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:home_care_services)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create home_care_service" do
    assert_difference('HomeCareService.count') do
      post :create, home_care_service: { exam_type_id: @home_care_service.exam_type_id, speciality_id: @home_care_service.speciality_id, user_id: @home_care_service.user_id, valor: @home_care_service.valor }
    end

    assert_redirected_to home_care_service_path(assigns(:home_care_service))
  end

  test "should show home_care_service" do
    get :show, id: @home_care_service
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @home_care_service
    assert_response :success
  end

  test "should update home_care_service" do
    patch :update, id: @home_care_service, home_care_service: { exam_type_id: @home_care_service.exam_type_id, speciality_id: @home_care_service.speciality_id, user_id: @home_care_service.user_id, valor: @home_care_service.valor }
    assert_redirected_to home_care_service_path(assigns(:home_care_service))
  end

  test "should destroy home_care_service" do
    assert_difference('HomeCareService.count', -1) do
      delete :destroy, id: @home_care_service
    end

    assert_redirected_to home_care_services_path
  end
end
