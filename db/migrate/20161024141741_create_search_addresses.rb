class CreateSearchAddresses < ActiveRecord::Migration
  def change
    create_table :search_addresses do |t|
      t.string :address
      t.string :complement
      t.string :latitude
      t.string :longitude
      t.references :user, index: true, foreign_key: true

      t.timestamps null: false
    end
  end
end
