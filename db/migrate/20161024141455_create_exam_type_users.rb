class CreateExamTypeUsers < ActiveRecord::Migration
  def change
    create_table :exam_types_users do |t|
      t.references :user, index: true, foreign_key: true
      t.references :exam_type, index: true, foreign_key: true

      t.timestamps null: false
    end
  end
end
