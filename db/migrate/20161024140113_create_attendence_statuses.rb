class CreateAttendenceStatuses < ActiveRecord::Migration
  def change
    create_table :attendence_statuses do |t|
      t.string :name
      t.string :doctor_message
      t.string :patient_message

      t.timestamps null: false
    end
  end
end
