class AddAttachmentClassificationsToAttendence < ActiveRecord::Migration
  def self.up
    change_table :attendences do |t|
     	t.references :classification, index: true, foreign_key: true
     	t.references :service_window, foreign_key: true
    end
  end

end
