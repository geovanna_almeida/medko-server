# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20161024143319) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"

  create_table "account_types", force: :cascade do |t|
    t.string   "name"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "attendence_statuses", force: :cascade do |t|
    t.string   "name"
    t.string   "doctor_message"
    t.string   "patient_message"
    t.datetime "created_at",      null: false
    t.datetime "updated_at",      null: false
  end

  create_table "attendence_types", force: :cascade do |t|
    t.string   "name"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "attendences", force: :cascade do |t|
    t.datetime "date"
    t.boolean  "home_care"
    t.float    "value"
    t.integer  "exam_type_id"
    t.integer  "speciality_id"
    t.integer  "place_id"
    t.integer  "search_address_id"
    t.integer  "payment_method_id"
    t.integer  "attendence_status_id"
    t.integer  "attendence_type_id"
    t.integer  "payment_id"
    t.integer  "health_plan_id"
    t.integer  "patient_id"
    t.integer  "doctor_id"
    t.datetime "created_at",           null: false
    t.datetime "updated_at",           null: false
    t.integer  "classification_id"
    t.integer  "service_window_id"
  end

  add_index "attendences", ["attendence_status_id"], name: "index_attendences_on_attendence_status_id", using: :btree
  add_index "attendences", ["attendence_type_id"], name: "index_attendences_on_attendence_type_id", using: :btree
  add_index "attendences", ["classification_id"], name: "index_attendences_on_classification_id", using: :btree
  add_index "attendences", ["exam_type_id"], name: "index_attendences_on_exam_type_id", using: :btree
  add_index "attendences", ["health_plan_id"], name: "index_attendences_on_health_plan_id", using: :btree
  add_index "attendences", ["payment_id"], name: "index_attendences_on_payment_id", using: :btree
  add_index "attendences", ["payment_method_id"], name: "index_attendences_on_payment_method_id", using: :btree
  add_index "attendences", ["place_id"], name: "index_attendences_on_place_id", using: :btree
  add_index "attendences", ["search_address_id"], name: "index_attendences_on_search_address_id", using: :btree
  add_index "attendences", ["speciality_id"], name: "index_attendences_on_speciality_id", using: :btree

  create_table "bank_accounts", force: :cascade do |t|
    t.string   "bank"
    t.string   "full_name"
    t.string   "cpf_cnpj"
    t.string   "account"
    t.string   "agency"
    t.string   "operation"
    t.integer  "user_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  add_index "bank_accounts", ["user_id"], name: "index_bank_accounts_on_user_id", using: :btree

  create_table "classifications", force: :cascade do |t|
    t.integer  "user_id"
    t.integer  "attendence_id"
    t.integer  "value"
    t.datetime "created_at",    null: false
    t.datetime "updated_at",    null: false
  end

  add_index "classifications", ["attendence_id"], name: "index_classifications_on_attendence_id", using: :btree
  add_index "classifications", ["user_id"], name: "index_classifications_on_user_id", using: :btree

  create_table "device_ids", force: :cascade do |t|
    t.string   "token"
    t.string   "os"
    t.string   "session_id"
    t.integer  "user_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  add_index "device_ids", ["user_id"], name: "index_device_ids_on_user_id", using: :btree

  create_table "exam_types", force: :cascade do |t|
    t.string   "name"
    t.boolean  "home_care"
    t.datetime "created_at",             null: false
    t.datetime "updated_at",             null: false
    t.string   "image_url_file_name"
    t.string   "image_url_content_type"
    t.integer  "image_url_file_size"
    t.datetime "image_url_updated_at"
  end

  create_table "exam_types_users", force: :cascade do |t|
    t.integer  "user_id"
    t.integer  "exam_type_id"
    t.datetime "created_at",   null: false
    t.datetime "updated_at",   null: false
  end

  add_index "exam_types_users", ["exam_type_id"], name: "index_exam_types_users_on_exam_type_id", using: :btree
  add_index "exam_types_users", ["user_id"], name: "index_exam_types_users_on_user_id", using: :btree

  create_table "health_plans", force: :cascade do |t|
    t.string   "name"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "health_plans_service_windows", force: :cascade do |t|
    t.integer  "service_window_id"
    t.integer  "health_plan_id"
    t.datetime "created_at",        null: false
    t.datetime "updated_at",        null: false
  end

  add_index "health_plans_service_windows", ["health_plan_id"], name: "index_health_plans_service_windows_on_health_plan_id", using: :btree
  add_index "health_plans_service_windows", ["service_window_id"], name: "index_health_plans_service_windows_on_service_window_id", using: :btree

  create_table "payment_methods", force: :cascade do |t|
    t.string   "name"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "payment_methods_service_windows", force: :cascade do |t|
    t.integer  "service_window_id"
    t.integer  "payment_method_id"
    t.datetime "created_at",        null: false
    t.datetime "updated_at",        null: false
  end

  add_index "payment_methods_service_windows", ["payment_method_id"], name: "index_payment_methods_service_windows_on_payment_method_id", using: :btree
  add_index "payment_methods_service_windows", ["service_window_id"], name: "index_payment_methods_service_windows_on_service_window_id", using: :btree

  create_table "payment_periods", force: :cascade do |t|
    t.date     "start_date"
    t.date     "end_date"
    t.float    "value"
    t.boolean  "accomplished"
    t.datetime "created_at",   null: false
    t.datetime "updated_at",   null: false
  end

  create_table "payments", force: :cascade do |t|
    t.float    "price"
    t.float    "receipt"
    t.integer  "payment_period_id"
    t.integer  "user_id"
    t.integer  "receipt_period_id"
    t.datetime "created_at",        null: false
    t.datetime "updated_at",        null: false
  end

  add_index "payments", ["payment_period_id"], name: "index_payments_on_payment_period_id", using: :btree
  add_index "payments", ["receipt_period_id"], name: "index_payments_on_receipt_period_id", using: :btree
  add_index "payments", ["user_id"], name: "index_payments_on_user_id", using: :btree

  create_table "places", force: :cascade do |t|
    t.string   "name"
    t.string   "address"
    t.string   "complement"
    t.float    "latitude"
    t.float    "longitude"
    t.integer  "user_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  add_index "places", ["user_id"], name: "index_places_on_user_id", using: :btree

  create_table "receipt_periods", force: :cascade do |t|
    t.date     "start_date"
    t.date     "end_date"
    t.float    "value"
    t.boolean  "accompliched"
    t.datetime "created_at",   null: false
    t.datetime "updated_at",   null: false
  end

  create_table "search_addresses", force: :cascade do |t|
    t.string   "address"
    t.string   "complement"
    t.string   "latitude"
    t.string   "longitude"
    t.integer  "user_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  add_index "search_addresses", ["user_id"], name: "index_search_addresses_on_user_id", using: :btree

  create_table "service_windows", force: :cascade do |t|
    t.integer  "user_id"
    t.integer  "week_day"
    t.time     "start_time"
    t.time     "end_time"
    t.integer  "service_time"
    t.float    "value"
    t.integer  "place_id"
    t.integer  "attendence_id"
    t.integer  "exam_type_id"
    t.integer  "speciality_id"
    t.datetime "created_at",    null: false
    t.datetime "updated_at",    null: false
  end

  add_index "service_windows", ["attendence_id"], name: "index_service_windows_on_attendence_id", using: :btree
  add_index "service_windows", ["exam_type_id"], name: "index_service_windows_on_exam_type_id", using: :btree
  add_index "service_windows", ["place_id"], name: "index_service_windows_on_place_id", using: :btree
  add_index "service_windows", ["speciality_id"], name: "index_service_windows_on_speciality_id", using: :btree
  add_index "service_windows", ["user_id"], name: "index_service_windows_on_user_id", using: :btree

  create_table "specialities", force: :cascade do |t|
    t.string   "name"
    t.boolean  "home_care"
    t.datetime "created_at",             null: false
    t.datetime "updated_at",             null: false
    t.string   "image_url_file_name"
    t.string   "image_url_content_type"
    t.integer  "image_url_file_size"
    t.datetime "image_url_updated_at"
  end

  create_table "specialities_users", force: :cascade do |t|
    t.integer  "user_id"
    t.integer  "speciality_id"
    t.datetime "created_at",    null: false
    t.datetime "updated_at",    null: false
  end

  add_index "specialities_users", ["speciality_id"], name: "index_specialities_users_on_speciality_id", using: :btree
  add_index "specialities_users", ["user_id"], name: "index_specialities_users_on_user_id", using: :btree

  create_table "users", force: :cascade do |t|
    t.string   "name"
    t.string   "password"
    t.string   "telephone"
    t.string   "crm"
    t.string   "full_name"
    t.string   "zip_code"
    t.string   "address"
    t.boolean  "active",                              default: true
    t.boolean  "blocked"
    t.boolean  "facebook_account"
    t.boolean  "rating"
    t.string   "facebook_token"
    t.string   "blocked_message"
    t.float    "latitude"
    t.float    "longitude"
    t.float    "value_home_care"
    t.datetime "date_location"
    t.integer  "account_type_id",                     default: 1
    t.datetime "created_at",                                         null: false
    t.datetime "updated_at",                                         null: false
    t.string   "email",                               default: "",   null: false
    t.string   "encrypted_password",                  default: "",   null: false
    t.string   "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.integer  "sign_in_count",                       default: 0,    null: false
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.string   "current_sign_in_ip"
    t.string   "last_sign_in_ip"
    t.string   "provider"
    t.string   "uid"
    t.string   "avatar_file_name"
    t.string   "avatar_content_type"
    t.integer  "avatar_file_size"
    t.datetime "avatar_updated_at"
    t.string   "crm_image_file_name"
    t.string   "crm_image_content_type"
    t.integer  "crm_image_file_size"
    t.datetime "crm_image_updated_at"
    t.string   "proof_of_address_image_file_name"
    t.string   "proof_of_address_image_content_type"
    t.integer  "proof_of_address_image_file_size"
    t.datetime "proof_of_address_image_updated_at"
  end

  add_index "users", ["account_type_id"], name: "index_users_on_account_type_id", using: :btree
  add_index "users", ["email"], name: "index_users_on_email", unique: true, using: :btree
  add_index "users", ["reset_password_token"], name: "index_users_on_reset_password_token", unique: true, using: :btree

  add_foreign_key "attendences", "attendence_statuses"
  add_foreign_key "attendences", "attendence_types"
  add_foreign_key "attendences", "classifications"
  add_foreign_key "attendences", "exam_types"
  add_foreign_key "attendences", "health_plans"
  add_foreign_key "attendences", "payment_methods"
  add_foreign_key "attendences", "payments"
  add_foreign_key "attendences", "places"
  add_foreign_key "attendences", "search_addresses"
  add_foreign_key "attendences", "service_windows"
  add_foreign_key "attendences", "specialities"
  add_foreign_key "bank_accounts", "users"
  add_foreign_key "classifications", "attendences"
  add_foreign_key "classifications", "users"
  add_foreign_key "device_ids", "users"
  add_foreign_key "exam_types_users", "exam_types"
  add_foreign_key "exam_types_users", "users"
  add_foreign_key "health_plans_service_windows", "health_plans"
  add_foreign_key "health_plans_service_windows", "service_windows"
  add_foreign_key "payment_methods_service_windows", "payment_methods"
  add_foreign_key "payment_methods_service_windows", "service_windows"
  add_foreign_key "payments", "payment_periods"
  add_foreign_key "payments", "receipt_periods"
  add_foreign_key "payments", "users"
  add_foreign_key "places", "users"
  add_foreign_key "search_addresses", "users"
  add_foreign_key "service_windows", "attendences"
  add_foreign_key "service_windows", "exam_types"
  add_foreign_key "service_windows", "places"
  add_foreign_key "service_windows", "specialities"
  add_foreign_key "service_windows", "users"
  add_foreign_key "specialities_users", "specialities"
  add_foreign_key "specialities_users", "users"
  add_foreign_key "users", "account_types"
end
