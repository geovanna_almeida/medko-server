Rails.application.routes.draw do

  # This line mounts Spree's routes at the root of your application.
  # This means, any requests to URLs such as /products, will go to Spree::ProductsController.
  # If you would like to change where this engine is mounted, simply change the :at option to something different.
  #
  # We ask that you don't use the :as option here, as Spree relies on it being the default of "spree"
        
  devise_for :users, :controllers => { :omniauth_callbacks => "users/omniauth_callbacks",
    :sessions => "users/sessions", :registrations => 'users/registrations'}


  resources :attendences, :except =>[:edit] 
  resources :users, only: [:show, :update,:create,:index,:edit]
  resources :places
  resources :specialities
  resources :search_addresses, :except =>[:destroy] 
  resources :exam_types
  resources :payment_methods, only: [:show]
  resources :health_plans, only: [:show]
  resources :attendence_types, only: [:show]
  resources :service_windows, :except =>[:edit] 
  resources :payments, :only => [:index, :new]
  resources :bank_account, only: [:show]
  resources :classifications, :except =>[:edit,:destroy] 

  get 'payment/billet' => 'payments#generate_billet'

  get 'attendence/search' => 'attendences#search'
  post 'attendence/search' => 'attendences#search'
  post'attendence/create_attendences' => 'attendences#create_attendences'
  post'attendence/search_attendences' => 'attendences#search_attendences'

  post 'pagseguro/notifications' => 'notifications#create'
  post 'attendence/confirm' => 'attendences#confirm'
  post 'attendence/cancel' => 'attendences#cancel'
  post 'attendence/time' => 'attendences#time'
  post 'attendence/pag_seguro' => 'attendences#pag_seguro'

  get 'admin/create' => 'users#new'

  post 'payment/confirm' => 'payments#confirm_payment'
  post 'payment/receipt' => 'payments#confirm_receipt'
  
 devise_scope :user do
  get 'cadastro/paciente' => 'users/registrations#paciente', :as => :new_usuario_registration
  get 'cadastro/medico' => 'users/registrations#medico'
  get 'cadastro/adm',to: 'users/registrations#adm'
end

  namespace :api do

    scope :users do
      post '/sign_in_mobile',to:'/api/sessions#create', defaults: { format: 'json' }
      post '/sign_in_mobile_facebook',to:'/api/sessions#create_facebook', defaults: { format: 'json' }
      delete '/sign_out_mobile',to:'/api/sessions#destroy'
      post '/reset_password',to:'/api/sessions#reset_password'
      post '/register_mobile_multipart',to:'/api/registrations#create_multipart', defaults: { format: 'json' }
      post '/register_mobile',to:'/api/registrations#create', defaults: { format: 'json' }
  end

    post '/points', to: '/api/points#location'

    get '/attendences',to:'/api/attendences#index', defaults: { format: 'json' }

    post '/classification',to:'/api/classifications#create', defaults: { format: 'json' }

    get '/attendences/:id',to:'/api/attendences#show', defaults: { format: 'json' }

    get '/attendences/show',to:'/api/attendences#show', defaults: { format: 'json' }

    post '/attendences/confirm',to:'/api/attendences#confirm', defaults: { format: 'json' }

    get '/attendences/payment/:id',to:'/api/attendences#payment', defaults: { format: 'json' }

    post '/attendences/cancel',to:'/api/attendences#cancel', defaults: { format: 'json' }

    post '/attendences/time',to:'/api/attendences#time', defaults: { format: 'json' }

    get '/specialities',to:'/api/specialities#index', defaults: { format: 'json' }

    get '/exam_types',to:'/api/exam_types#index', defaults: { format: 'json' }

    get '/payment_methods',to:'/api/payment_methods#index', defaults: { format: 'json' }

    get '/health_plans',to:'/api/health_plans#index', defaults: { format: 'json' }

    get '/attendence_types',to:'/api/attendence_types#index', defaults: { format: 'json' }

    post '/device_id',to:'/api/device_ids#create', defaults: { format: 'json' } 

    get '/current_user',to:'/api/users#current_user_session', defaults: { format: 'json' }

    post '/attendences/search_attendence_home_care',to:'/api/attendences#search_attendences_home_care', defaults: { format: 'json' }    
 
    post '/attendences/search_attendence_clinic',to:'/api/attendences#search_attendences_clinic', defaults: { format: 'json' }

    post '/attendences/create_attendence',to:'/api/attendences#create_attendences', defaults: { format: 'json' }

  end

 
  root :to => 'attendences#index'


end
